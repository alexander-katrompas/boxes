#ifndef BOXES_MAIN_H
#define BOXES_MAIN_H

#include <iostream>
#include "rectangle.h"
#include "box.h"

using namespace std;


void displayRectangle(rectangle*);

void displayBox(box*);


#endif //BOXES_MAIN_H
